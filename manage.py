import logging
from werkzeug.contrib.profiler import ProfilerMiddleware

from flask import request
from flask_migrate import Migrate, MigrateCommand
from flask_script import Manager, Server, Shell

from api import create_app, db

from api.v1.models.histories import (
    HistoryModel
)

from api.v1.models.users import (
    UserModel,
    PermissionModel,
    RoleModel
)

from api.v1.schemas.users import (
    PermissionSchema,
    RoleSchema,
    UserSchema
)

app = create_app('default')
manager = Manager(app)
migrate = Migrate(app, db)

app.config['PROFILE'] = True
app.wsgi_app = ProfilerMiddleware(app.wsgi_app, restrictions=[30])

logger = logging.getLogger()
logger.addHandler(logging.StreamHandler())

@app.before_request
def option_autoreply():
    """ Always reply 200 on OPTIONS request """

    if request.method == 'OPTIONS':
        resp = app.make_default_options_response()
        headers = None
        if 'ACCESS_CONTROL_REQUEST_HEADERS' in request.headers:
            headers = request.headers['ACCESS_CONTROL_REQUEST_HEADERS']

        h = resp.headers

        h['Access-Control-Allow-Credentials'] = "true"

        # Allow the origin which made the XHR
        h['Access-Control-Allow-Origin'] = request.headers['Origin']
        # Allow the actual method
        h['Access-Control-Allow-Methods'] = request.headers['Access-Control-Request-Method']
        # Allow for 10 seconds
        h['Access-Control-Max-Age'] = "10"

        # We also keep current headers
        if headers is not None:
            h['Access-Control-Allow-Headers'] = headers

        return resp


@app.after_request
def set_allow_origin(resp):
    """ Set origin for GET, POST, PUT, DELETE requests """

    h = resp.headers

    # Allow crossdomain for other HTTP Verbs
    if request.method != 'OPTIONS' and 'Origin' in request.headers:
        h['Access-Control-Allow-Origin'] = request.headers['Origin']

    return resp


def shell_context():
    return dict(
        app=app,
        db=db
    )

manager.add_command('shell', Shell(make_context=shell_context))
manager.add_command('db', MigrateCommand)
manager.add_command('runserver', Server(
    threaded=True,
    use_reloader=True,
    use_debugger=True,
    host='api.microuser',
    port=3000
))


@manager.command
def test():
    import unittest
    tests = unittest.TestLoader().discover('tests')
    unittest.TextTestRunner(verbosity=2).run(tests)


if __name__ == '__main__':
    manager.run()
