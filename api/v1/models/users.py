import base64
import binascii
import datetime
import hashlib
import hmac
import json
import os
import re
import time
from collections import OrderedDict

import arrow
import bleach
from flask import current_app
from slugify import slugify
from sqlalchemy import asc, desc, exc
from sqlalchemy.sql.expression import and_, func, or_
from sqlalchemy.ext.mutable import MutableDict
from sqlalchemy_utils import JSONType
from flask_sqlalchemy_caching import FromCache, RelationshipCache

from api import bcrypt, cache, db
from api.v1.models import SearchableMixin
from api.v1.models.histories import MicroUserHistory, HistoryModel
from api.v1.tasks import send_celery_email_queue, send_celery_sms_queue
from api.v1.utils import get_clients_ip, generate_sms_token, reverse_id, normalize_phone_number


class PermissionModel(db.Model):
    
    __tablename__ = 'micro_permissions'

    id = db.Column(db.Integer, primary_key=True, index=True, unique=True, autoincrement=True)
    name = db.Column(db.String(50), unique=True, nullable=False)
    slug = db.Column(db.String(50), unique=True,nullable=False)
    description = db.Column(db.String(255), nullable=True)
    created = db.Column(db.DateTime, default=datetime.datetime.utcnow)
    updated = db.Column(db.DateTime, default=datetime.datetime.utcnow, onupdate=datetime.datetime.utcnow)

    def __init__(self, **kwargs):
        super(PermissionModel, self).__init__(**kwargs)

    def __repr__(self):
        return '<Permission %r>' % self.name

    @staticmethod
    def count_all(cls, q=None):
        
        query = cls.query

        if q:
            query = query.filter(cls.name.like('%'+q+'%'))

        cached_query = query.options(FromCache(cache))
        return cached_query.count()


    @classmethod
    def get_all(cls, q=None, page=1, perpage=25):
        
        query = cls.query

        offset = (page - 1) * perpage

        if q:
            query = query.filter(cls.name.like('%'+q+'%'))

        query = query.order_by(cls.name.asc()).limit(perpage).offset(offset).all()
        cached_query = query.options(FromCache(cache))
        return cached_query.all()


    @classmethod
    def get_by(cls, id):
        
        query = cls.query
        cached_query = query.options(FromCache(cache))
        return cached_query.get(id)


class RoleModel(db.Model):
    
    __tablename__ = "micro_roles"

    id = db.Column(db.Integer, primary_key=True, index=True, unique=True, autoincrement=True)
    name = db.Column(db.String(100), unique=True, nullable=False)
    slug = db.Column(db.String(100), unique=True, nullable=False)
    description = db.Column(db.String(255), nullable=True)
    permissions = db.Column(MutableDict.as_mutable(JSONType), nullable=False)
    created = db.Column(db.DateTime, default=datetime.datetime.utcnow)
    updated = db.Column(db.DateTime, default=datetime.datetime.utcnow, onupdate=datetime.datetime.utcnow)
    users = db.relationship('UserModel', backref='user_role', lazy='dynamic', cascade="all, delete-orphan")

    def __init__(self, **kwargs):
        super(RoleModel, self).__init__(**kwargs)

    def __repr__(self):
        return '<Role %r>' % self.name


    @classmethod
    def count_all(cls, q=None):
        
        query = cls.query

        if q:
            query = query.filter(cls.name.like('%'+q+'%'))

        cached_query = query.options(FromCache(cache))
        return cached_query.count()


    @classmethod
    def get_all(cls, q=None, page=1, perpage=25):
        
        query = cls.query

        offset = (page - 1) * perpage

        if q:
            query = query.filter(cls.name.like('%'+q+'%'))

        query = query.order_by(cls.name.asc()).limit(perpage).offset(offset)
        cached_query = query.options(FromCache(cache))
        return cached_query.all()


    @classmethod
    def get_by(cls, id):
        
        query = cls.query
        cached_query = query.options(FromCache(cache))
        return cached_query.get(id)



class UserModel(SearchableMixin, db.Model):
    
    STATUS = OrderedDict([('unverified', 'Unverified'), ('verified', 'Verified'), ('banned', 'Banned')])

    __tablename__ = "micro_users"

    __searchable__ = ['fullname', 'phone', 'email']

    id = db.Column(db.Integer, primary_key=True, index=True, unique=True, autoincrement=True)
    fullname = db.Column(db.String(45), nullable=False)
    phone = db.Column(db.String(25), nullable=False)
    email = db.Column(db.String(45), nullable=False)
    _password = db.Column('password', db.String(128), nullable=True)    
    passkey = db.Column(db.String(6), nullable=True)
    passkey_expire = db.Column(db.Integer, nullable=True)
    verification_code = db.Column(db.String(128), nullable=True)
    activation_code = db.Column(db.String(128), nullable=True)
    activation_expire = db.Column(db.Integer, nullable=True)
    forgot_code = db.Column(db.String(128), nullable=True)
    forgot_expire = db.Column(db.Integer, nullable=True)
    status = db.Column(db.Enum(*STATUS, name="user_status"), index=True, default='unverified')
    created = db.Column(db.DateTime, default=datetime.datetime.utcnow)
    updated = db.Column(db.DateTime, default=datetime.datetime.utcnow, onupdate=datetime.datetime.utcnow)
    role_id = db.Column(db.Integer, db.ForeignKey('micro_roles.id'), index=True)
    histories = db.relationship('HistoryModel', backref='history_user', lazy='dynamic', cascade='all, delete-orphan')


    def __init__(self, **kwargs):
        super(UserModel, self).__init__(**kwargs)


    def __repr__(self):
        return '<User %r, %r, %r, %r>' % (self.id, self.fullname, self.phone, self.email)

    @property
    def password(self):
        raise AttributeError('Password is not a readable attribute')

    @password.setter
    def password(self, password):
        self._password = bcrypt.generate_password_hash(self._password, password)

    def check_password(self, password):
        return bcrypt.check_password_hash(self._password, password)

    def check_permission(self, key, value):
        permissions = self.user_role.permissions
        if key in permissions and permissions[key][value]:
            return True
        return False

    def last_histories(self):
        return self.histories.order_by(HistoryModel.created.desc()).limit(10).offset(0).all()


    def verify(self, params={}):

        nextminutes = datetime.datetime.now() + datetime.timedelta(minutes=5)
        passkey_expire = time.mktime(nextminutes.timetuple())
        passkey = generate_sms_token(self.phone)

        self.passkey = passkey
        self.passkey_expire = passkey_expire

        secret = str(time.time()*1000) + self.phone + get_clients_ip() + binascii.b2a_hex(os.urandom(100))
        signature = hmac.new(current_app.config.get('SECRET_KEY'), secret, hashlib.sha256).hexdigest()

        self.verification_code = signature

        payload = {
            'module': 'user',
            'signature': signature,
            'expire': passkey_expire,
            'params': params
        }

        verification_code = base64.urlsafe_b64encode(json.dumps(payload))
        db.session.commit()

        #if self.phone:
        #    send_celery_sms_queue.delay({
        #        "phonenumber":self.phone,
        #        "content":'Kode Micro anda adalah ' + passkey + ', berlaku sampai dengan 5 menit.'
        #    })

        if self.email:
            send_celery_email_queue.delay({
                'subject': 'Verifikasi Akun Micro',
                'to': self.email,
                'template': 'emails/verification',
                'variables': {
                    'fullname': self.fullname,
                    'passkey': passkey,
                    'expired': '5 Menit'
                }
            })

        return {
            'code': verification_code,
            'expired': passkey_expire
        }


    @classmethod
    def is_exist(cls, id=None, email=None, phone=None):

        query = cls.query

        if id:
            query = query.filter(cls.id != id)

        if email:
            query = query.filter(cls.email==email)

        if phone:
            query = query.filter(cls.phone==phone)

        cached_query = query.options(FromCache(cache))
        return cached_query.first()

    
    @classmethod
    def count_all(cls, q=None, status=None, role_id=None):
        
        query = cls.query

        if q:
            query = query.filter(or_(cls.fullname.like('%'+q+'%'), cls.phone.like('%'+q+'%')))

        if status:
            query = query.filter(cls.status == status)

        if role_id:
            query = query.filter(cls.role_id == role_id)

        cached_query = query.options(FromCache(cache))
        return cached_query.count()


    @classmethod
    def get_all(cls, q=None, status=None, role_id=None, page=1, perpage=10):

        offset = (page - 1) * perpage
        
        query = cls.query        

        if q:
            query = query.filter(or_(cls.fullname.like('%'+q+'%'), cls.phone.like('%'+q+'%')))

        if status:
            query = query.filter(cls.status == status)

        if role_id:
            query = query.filter(cls.role_id == role_id)

        query = query.order_by(cls.fullname.asc()).limit(perpage).offset(offset)
        cached_query = query.options(FromCache(cache))
        return cached_query.all()


    @classmethod
    def get_by(cls, id=None, phone=None, email=None):
        
        query = cls.query

        if id:
            query = query.filter(cls.id == id)

        if phone:
            query = query.filter(cls.phone == phone)

        if email:
            query = query.filter(cls.email == email)

        cached_query = query.options(FromCache(cache))
        return cached_query.first()


MicroUserHistory(PermissionModel)
MicroUserHistory(RoleModel)
MicroUserHistory(UserModel)
